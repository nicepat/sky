/* Write a program to Validate an IPv4 Address.
According to Wikipedia, IPv4 addresses are canonically represented in dot-decimal notation, which consists of four decimal numbers, each ranging from 0 to 255, separated by dots, e.g., 172.16.254.1 .
A valid IPv4 Address is of the form x1.x2.x3.x4 where 0 <= (x1, x2, x3, x4) <= 255.
Thus, we can write the generalized form of an IPv4 address as (0-255).(0-255).(0-255).(0-255).
*/
#include <stdio.h>
#include <string.h>

int is_valid(char address[]) {

    int classes = 4;
    if (strcmp(address, "") == 0) {   // check if the IP address string is empty.
        return 0;
    }

    for (int i = 0; i < strlen(address); i++) {
        if (address[i] != '.') {
            if (address[i]  < '0' || address[i] > '9') {
                return 0;
            }
        }
    }

    char * token = strtok(address, ".");   // check the first part of the IP address.

    int partial = atoi(token);
    if ((partial < 0) || (partial >= 255)) {
        return 0;
    }
    classes--;

    while(token != NULL) {   // proceed to the rest of IP address.
        token = strtok(NULL, ".");
        if ((token == NULL) && (classes >=0)) {  // check if IP address does not consist of 4 groups like 192.10.1
            return 0;
        }
        partial = atoi(token);
        if ((partial < 0) || (partial >= 255)) {
            return 0;
        }
        classes--;
        if (classes == 0) {   // end of IP address.
            break;
        }
    }

    if (classes == -1) {   // check if IP address has more than 4 groups like 10.2.22.12.3
        return 0;
    }

    return 1;
}

int main() {
    //char ip_address[] = "222.111.111";
    //char ip_address[] = "192.168.877.111";
    char ip_address[] = "10.0.0.1";

    printf("IP address: %s\n", ip_address);

    if (is_valid(ip_address) == 1) {
        printf("The IP address is valid\n");
    } else {
        printf("The IP address is not valid\n");
    }
}