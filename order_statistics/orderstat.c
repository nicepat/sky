#include <stdio.h>

int main() {
    int my_input_arr[] = {8,1,3,5,8,12,2};
    int arr_size = sizeof(my_input_arr)/sizeof(int);
    printf("The array size: %d\n", arr_size);
    int min_val;

    for(int i = 0; i < arr_size; i++) {
        if (i == 0) {
            min_val = my_input_arr[i];
        } else {
            if (my_input_arr[i] < min_val) {
                min_val = my_input_arr[i];
            }
        }
    }
    printf("Find the min val: %d\n", min_val);
}