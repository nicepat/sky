/* Given a sorted array of integers, return the low and high index of the given key.
You must return -1 if the indexes are not found
*/
#include <stdio.h>
#define ARR_SIZE 14

int find_low_index(int my_array[], int key) {
    int low = 0;
    int high = sizeof(my_array)/sizeof(int) - 1;
    int mid = high / 2;

    while (low <= high) {
        int mid_val = my_array[mid];

        if (mid_val < key) {
            low = mid + 1;
        } else {
            high = mid - 1;
        }
        mid = low + (high - low)/2;
    }
    if ((low < sizeof(my_array)/sizeof(int)) && (my_array[low] == key)) {
        return low;
    }

    return -1;
}

int find_high_index(int my_array[], int key) {
    int low = 0;
    int high = sizeof(my_array)/sizeof(int) - 1;
    int mid = high / 2;

    while (low <= high) {
        int mid_val = my_array[mid];

        if (mid_val <= key) {
            low = mid + 1;
        } else {
            high = mid - 1;
        }
        mid = low + (high - low)/2;
    }

    if ( high == -1) {
        return high;
    }

    if ((high < sizeof(my_array)/sizeof(int)) && (my_array[high] == key)) {
        return high;
    }

    return -1;
}

int main() {
    int arr_val[ARR_SIZE] = {1,2,5,5,5,5,5,5,5,5,20,20,20,50};
    int key = 5;

    int low = find_low_index(arr_val, key);
    int high = find_high_index(arr_val, key);

    printf("low value index: %d\n", low);
    printf("high value index: %d\n", high);
}
