#!/bin/bash
# Original ACR name
original_acr_name=$1

# Backup ACR name in the same subscription
backup_acr_name=$2
subscription=$3

# For the backup procedure, this value is 1. Otherwise, 0.
is_backup=$4

is_debug=0

# If this is recovery, then swap the names.
if [ $is_backup != 0 ]; then
    temp=$original_acr_name
    original_acr_name=$backup_acr_name
    backup_acr_name=$temp
# Upon backup time, we only recover a single repo for quick response.
    if [ ! -z $4 ]; then
        repoName=$4
        echo $repoName > delete_repo_names.txt
    fi
fi

while IFS= read -r repoName
do
    az acr repository show-tags --name $original_acr_name --repository $repoName --orderby time_asc  -o tsv --detail --subscription $subscription > $repoName.txt
    while IFS= read -r line
    do
        timestamp=$(echo $line | cut -d " " -f1)
        manifest=$(echo $line | cut -d " " -f2)
        tag=$(echo $line | cut -d " " -f4)
        docker pull $original_acr_name.azurecr.io/$repoName:$tag
        docker tag $original_acr_name.azurecr.io/$repoName:$tag $backup_acr_name.azurecr.io/$repoName:$tag
        docker push $backup_acr_name.azurecr.io/$repoName:$tag
        if [ $? != 0 ]; then
            echo "[FAILED] docker push failed - $backup_acr_name.azurecr.io/$acr_name:$tag"
        else
            echo "[PASSED] docker push succeeded - $backup_acr_name.azurecr.io/$acr_name:$tag"
        fi

    done < "$repoName.txt"
done < "delete_repo_names.txt"

echo "Image copying operation completed, but repository removal is not done. Manually delete the repo, if needed."
