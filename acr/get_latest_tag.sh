#!/bin/bash
# get_latest_tag.sh <acr name without azurecr.io>

az acr repository list --name $1 -o tsv > repo.txt

while IFS= read -r repoName
do
    az acr repository show-tags -n $1 --repository $repoName --top 1 --orderby time_desc --detail -o tsv >> $(date +"%m-%d-%y").txt
done < "repo.txt"
echo done