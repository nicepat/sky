#!/bin/bash
# This script deletes PR images if they are older than 30-days.
# Open WSL or Linux terminal with the az cli installed. Calling this script requires subscription and ACR name.
# Example, $delete_acr_tags.sh 12345678-abcd-1234-12zx-123456thgfds container_registry_name
# It used to have long run-time.
#
set -euxo pipefail

if [ $# -lt 2 ]; then
    echo "Error - missing arguments"
    echo "[Usage]: $0 acr_name subscription_id"
    exit 1
fi

acr_name=$1
subscription_id=$2

# It supports multiple acr_names in parallel.
echo "Save all repo names from ACR $acr_name to delete_acr_target_names"
az acr repository list --name "$acr_name" --output table --subscription "$subscription_id" > delete_acr_target_names.txt

# Removed the first 2 lines from the original output
sed -i "1,2d" delete_acr_target_names.txt

# Create the original copy for recovery purpose
cp delete_acr_target_names.txt delete_acr_target_names.original

# The number of execution bash scripts
filecount=1
exfile="executionfile_${filecount}.sh"
# The number of repos per single execution bash script
repocount=5

touch sum.txt

# Generate the execution files
while IFS= read -r repoName
do
    myline=$(az acr repository show-tags --name "$acr_name" --repository "$repoName" --orderby time_asc -o tsv --detail | wc -l)
	echo "$repoName $myline" >> sum.txt
done < "delete_acr_target_names.txt"
