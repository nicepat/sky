# Git clone or git pull for sync the master branch from the remote branch.
# Need one argument versions.yml file path and get its history
git log --pretty=oneline $1 > git-log-output.txt

while IFS= read -r commit
do
    echo $commit
done < "git-log-output.txt"

# git reset to each commit

# loop for all container versions <- there are some container images are mismatching name.
# add the versions in the array of each container image. (looks like hash table)

