/* Given an array of size N-1 such that it only contains distinct integers in the range of 1 to N.
Find the missing element.
*/

#include <stdio.h>

void print_array(int arr[], int size) {
    for(int i = 0; i < (size-1); i++) {
        printf("%d ",arr[i]);
    }
    printf("\n");
    return;
}
int main() {

    int n = 10;
    int arr_val[] = {6,1,2,8,3,4,7,10,5};
    int tmp;

    print_array(arr_val, n);

    for(int i = 0; i < (n - 1); i++) {
        for(int j = (i + 1); j < (n - 1); j++) {
            if(arr_val[i] < arr_val[j]) {
                tmp = arr_val[i];
                arr_val[i] = arr_val[j];
                arr_val[j] = tmp;
            }
        }
    }

    print_array(arr_val, n);

    for (int i = 0; i < (n - 2); i++) {
        if (arr_val[i] != arr_val[i+1] + 1) {
            printf("Found the missing number: %d\n", (arr_val[i] - 1));
        }
    }
}