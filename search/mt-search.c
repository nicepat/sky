#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>

#define MAX 990000000
#define THREAD_MAX  4

int myarr [MAX];
int key = 7;
int flag = 0;
int current_thread = 0;

void* thread_search(void* args) {
    int num = current_thread++;
    printf("%d-%d\n", num * (MAX/THREAD_MAX), ((num + 1)*(MAX/THREAD_MAX)));
    for (int i = num * (MAX/THREAD_MAX); i < ((num + 1)*(MAX/THREAD_MAX)); i++){
        //printf("%d - %d\n", num, myarr[i]);
        if (myarr[i] == key)
            flag = 1;
    }
}

int main() {
    printf("Creating %d sized array with integer numbers under 50\n", MAX);
    pthread_t thread[THREAD_MAX];
    time_t start_time = time(NULL);

    // add the random number to each array item.
    for (int i = 0; i < MAX; i++) {
        myarr[i] = rand() % 50;
    }

    printf("Searching key %d ...\n", key);

    // create more threads
    for (int i = 0; i < THREAD_MAX; i++) {
        pthread_create(&thread[i], NULL, thread_search, (void*)NULL);
    }

    // the job done, and then clean up
    for (int i = 0; i < THREAD_MAX; i++) {
        pthread_join(thread[i], NULL);
    }

    // result
    if (flag == 1)
        printf("found the key in the list\n");
    else
        printf("not found the key :( \n");
    time_t end_time = time(NULL);
    double delta = difftime(end_time, start_time);
    printf("Starting time -    %15lu (sec)\n", start_time);
    printf("Ending time time - %15lu (sec)\n", end_time);
    printf("Elapsed time -       %15f (sec)\n", delta);
}