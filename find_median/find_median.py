#!/usr/bin/python

# Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.
import sys

num1 = [1,2,5,6]
num2 = [3,4,8,9]

def main():
    num3 = num1 + num2
    num3.sort()
    median = int(len(num3)/2)
    print(num3)
    print(num3[median])
    return


if __name__ == "__main__":
    main()