// Given an array of integers and a value, determine if there are any two integers in the array whose sum is equal to the given value
#include <stdio.h>

int main() {
    int target_val = 10;
    int arr_val[] = {5,7,1,8,9,4,4};
    int arr_size = sizeof(arr_val)/sizeof(int);
    int delta = 0;
    int found = 0;
    int i,j;

    for(i = 0; i < arr_size; i++) {
        if (arr_val[i] >= target_val) {
            printf("Can not find the second value because the first value, %d, is greater than or equal to the target value, %d\n", arr_val[i], target_val);
            break;
        }
        delta = target_val - arr_val[i];

        if (i == (arr_size-1)) {
            break;
        }

        for(j = i+1; j < arr_size; j++) {
            if (arr_val[j] == delta) {
                found = 1;
                break;
            }
        }
        if (found == 1) {
            printf("found the values making a sum, %d - %d and %d\n", target_val, arr_val[i], arr_val[j]);
            break;
        }
    }
    if (found == 0) {
        printf("Not found the values make a sum %d\n", target_val);
    }
}