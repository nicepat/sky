/* Given two numbers as strings s1 and s2. Calculate their product.
*/

#include <stdio.h>

int main() {
    char input1[] = "444";
    char input2[] = "2";

    int sum = atoi(input1) * atoi(input2);
    printf("%s * %s = %d\n", input1, input2, sum);

}