// Move all zeros to the left of an array while maintaining its order.
#include <stdio.h>

int main() {
    int arr_val[] = {1,10,20,0,59,63,0,88,0};  // expected output is 0 0 0 1 10 20 59 63 88
    int idx_last_non_zero = -1;
    int arr_size = sizeof(arr_val)/sizeof(int);
    int tmp;

    for(int i = 0; i < arr_size; i++) {
        if (arr_val[i] == 0) {
            idx_last_non_zero++;
            for (int k = i; k > idx_last_non_zero; k--){
                arr_val[k] = arr_val[k - 1];
                arr_val[k - 1] = 0;
            }
        }
    }

    for(int j = 0; j < arr_size; j++) {
        printf("%d ", arr_val[j]);
    }
    printf("\n");
}