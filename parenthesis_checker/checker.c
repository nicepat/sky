/* Given an expression string x.
Examine whether the pairs and the orders of “{“,”}”,”(“,”)”,”[“,”]” are correct in exp.
*/

#include <stdio.h>
#include <string.h>
#define SIZE 4   // Change to 4 or 16

char arr_val[SIZE] = {'[','(',']',')'};   // bad data
//char arr_val[SIZE] = {'[','(',')',']','{','}','{','[','(',')','(',')',']','(',')','}'};   /good data
char my_stack[SIZE] = {};
int top = 0;
char tmp;


void print_stack() {
    printf("STACK:");
    for(int i = 0; i < SIZE; i++) {
        printf("%c ", my_stack[i]);
    }
    printf(" --- top:%d\n", top);
}

void push(char entry) {
    my_stack[top++] = entry;
    printf("entry push  - %c\n", entry);
}

char pop() {
    top--;
    char tmp = my_stack[top];
    my_stack[top] = '\0';
    printf("entry pop - %c\n", tmp);
    return(tmp);

}

int main() {

    char last_push_char;

    for(int i = 0; i < SIZE; i++) {
        printf("iteration %d checks out this char %c\n", i, arr_val[i]);
        if ((arr_val[i] == '[') || (arr_val[i] == '{') || (arr_val[i] == '(')) {
            push(arr_val[i]);
            last_push_char = arr_val[i];
        } else {
            tmp = pop();

            if (tmp == '(') {
                if (arr_val[i] != ')') {
                    printf("Bad1: popped %c, held %c\n", tmp, arr_val[i]);
                    break;
                }
            }

            if (tmp == '[') {
                if (arr_val[i] != ']') {
                    printf("Bad2: popped %c, held %c\n", tmp, arr_val[i]);
                    break;
                }
            }

            if (tmp == '{') {
                if (arr_val[i] != '}') {
                    printf("Bad3: popped %c, held %c\n", tmp, arr_val[i]);
                    break;
                }
            }
        }
    }
}