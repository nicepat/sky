/* Given two strings A and B.
Find the minimum number of steps required to transform string A into string B.
The only allowed operation for the transformation is selecting a character from string A and inserting it in the beginning of string A
*/
#include <stdio.h>
#include<string.h>

int transform(char * s1, char * s2) {
    int steps = 0;
    int size1 = strlen(s1);
    int size2 = strlen(s2);

    if (size1 != size2) {
        return -1;
    }

    for(int i = 0; i < size1; i++) {
        steps += s1[i] - s2[i];
    }

    if (steps != 0) {
        return -1;
    }

    int i = size1 - 1;
    int j = size2 - 1;
    // Because this reads from backside, it would count the steps from back.
    while(i >= 0 && j >= 0) {
        if(s1[i] == s2[j]) {
            j--;
        }
        i--;
    }

    return j + 1;

}

int main() {
    char input_a[] = "bad";
    char input_b[] = "adb";

    printf("The min steps required for string transformation: %d\n", transform(input_a, input_b));
}
