#include <stdio.h>
#include <stdlib.h>

# define MAX_NODE 1000

struct node {
    int number;
    struct node* next;
};

// Find the loop existence in the linked list

int main() {

    struct node* head = NULL;
    struct node* tail = NULL;
    struct node* tmp = NULL;

    for (int i = 0; i < MAX_NODE; i++) {
        tmp = (struct node*)malloc(sizeof(struct node));
        tmp->number = rand();
        tmp->next = NULL;
        if (head == NULL) {
            head = tmp;
            tail = tmp;
        } else {
            tail->next = tmp;
            tail = tmp;
        }
    }

    // print the whole list
    tmp = head;
    int data_index = 1;

    while (tmp->next) {
        printf("The data is %d at %d\n", tmp->number, data_index++);
        tmp = tmp->next;
    }

    // make a loop from 1 to 4
    tmp = head;
    while (tmp->next) {
        tmp = tmp->next;
    }
    tmp->next = head; // This is a part for making a loop, a circle

    // print the while list. Should be infite loop.
    tmp = head;
    while (tmp->next) {
        printf("The data is %d\n", tmp->number);
        tmp = tmp->next;
        if (tmp == head) {
            printf("found the loop. Terminate the operation.\n");
            break;
        }
    }

}