#!/bin/bash
# Print the each string reverse way in the sentence.

for i in This is the sentence.
do
    output=$(echo $i | rev)
    if [[ ${output:0:1} == '.' ]]; then
        output=${output:1}"."
    fi
    tmp=$tmp" "$(echo $output)
done
echo $tmp