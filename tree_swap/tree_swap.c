/*
Given the root node of a binary tree, swap the 'left' and 'right' children for each node.
*/

#include <stdio.h>
#include <stdlib.h>

struct node {
    int key;
    struct node * left;
    struct node * right;
};

struct node * head;

struct node * get_new_node(int key) {
    struct node * tmp;
    tmp = (struct node *)malloc(sizeof(struct node));
    tmp->key = key;
    tmp->left = NULL;
    tmp->right = NULL;
    return tmp;
}

void print_tree(struct node * tmp, int pos, int lvl) {

    if (tmp == NULL) {
        return;
    }

    printf("%3d - %2d - %2d\n", tmp->key, pos, lvl);

    print_tree(tmp->left, -1, (lvl+1));

    print_tree(tmp->right, 1, (lvl+1));
}

void swap_tree(struct node * tmp) {
    struct node * swap_tmp;

    if (!tmp) {
        return;
    }

    swap_tmp = tmp->left;
    tmp->left = tmp->right;
    tmp->right = swap_tmp;

    swap_tree(tmp->left);
    swap_tree(tmp->right);
}

int main() {

    //A tree creation
    struct node * tmp = NULL;

    tmp = get_new_node(20);
    head = tmp;

    tmp = get_new_node(50);
    head->left = tmp;

    tmp = get_new_node(200);
    head->right = tmp;

    tmp = get_new_node(75);
    head->left->left = tmp;

    tmp = get_new_node(25);
    head->left->right = tmp;

    tmp = get_new_node(300);
    head->right->right = tmp;

    print_tree(head, 0, 0);

    swap_tree(head);
    printf("After swap the tree\n");

    print_tree(head, 0, 0);
}