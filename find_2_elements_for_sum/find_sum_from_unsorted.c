// Find 2 parameters from the list to make the sum value.
// Given an arrary with the signed integers, you will find if the array will find 2 parameters making the given sum values.
// [1,5,3,9], sum = 8
// [4,3,1,4], sum = 8 But they are not sorted.
// Here the first array has no parameter making the sume 8, the second array does, 4 and 4.
// This array is NOT sorted one.
#include <stdio.h>

int main() {
    int arr_data[] = {6,1,7,9,6};
    int com_idx = 0;
    int com_value;
    int exp_sum = 7;
    int is_possible = 0;

    int arr_size = sizeof(arr_data)/sizeof(int);
    int arr_comp[arr_size];

    for(int i = 0; i < arr_size; i++) {
        com_value = exp_sum - arr_data[i];  // We have to use complementary number from the linear search. O(n)
        if (com_idx > 0) {
            for(int j = 0; j < com_idx; j++) {
                if (arr_comp[j] == arr_data[i]) {
                    printf("found %d and %d\n", arr_data[i], com_value);
                    is_possible = 1;
                    break;
                }
            }
        }
        if (is_possible == 1) {
            break;
        }
        arr_comp[com_idx++] = com_value;
    }

    if (is_possible == 0) {
        printf("This array does not have matching elements making sum of %d\n", exp_sum);
    }
}