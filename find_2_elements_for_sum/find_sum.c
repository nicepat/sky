// Find 2 parameters from the list to make the sum value.
// Given an arrary with the signed integers, you will find if the array will find 2 parameters making the given sum values.
// [1,2,3,9], sum = 8
// [1,2,4,4], sum = 8
// Here the first array has no parameter making the sume 8, the second array does, 4 and 4.
// This array is sorted one.
#include <stdio.h>

int main() {
    int arr_data[] = {1,2,3,4,5};
    int exp_sum = 7;
    int is_possible = 0;

    int arr_size = sizeof(arr_data)/sizeof(int);
    int small_idx = 0;
    int large_idx = arr_size - 1;

    while(small_idx <= large_idx){
        if ((arr_data[small_idx] + arr_data[large_idx]) == exp_sum) {
            is_possible = 1;
            printf("Found them %d and %d\n", arr_data[small_idx], arr_data[large_idx]);
            break;
        } else if ((arr_data[small_idx] + arr_data[large_idx]) > exp_sum) {
            large_idx--;
        } else {
            small_idx++;
        }
    }

    if (is_possible == 0) {
        printf("This array does not have matching elements making sum of %d\n", exp_sum);
    }
}