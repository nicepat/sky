#!/usr/bin/python

# Given a string containing just the characters '(' and ')',
# find the length of the longest valid (well-formed) parentheses substring
# Assumption: there is no duplication in the series of parentheses like ((((())))).
# Above case outputs ().

import sys

def main():
#    my_entry = ")()())"
#    my_entry = ""
    my_entry = "(()"
    my_output = []
    is_open = 0;

    for p in my_entry:
        if ( p == "("):
            is_open = 1
        else:
            if (is_open == 1):
                my_output.append('()')
                is_open = 0

    print(my_output)

if __name__ == "__main__":
    main()