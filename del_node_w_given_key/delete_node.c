// You are given the head of a linked list and a key.
// You have to delete the node that contains this given key
#include <stdio.h>
#include <stdlib.h>

struct node {
    int key;
    struct node * next;
};

struct node * head = NULL;

struct node * get_node(int key_val)
{
    struct node * tmp = NULL;
    tmp = (struct node *)malloc(sizeof(struct node));
    tmp->key = key_val;
    tmp->next = NULL;

    return tmp;
};

void display_nodes(struct node *tmp) {
    while (tmp) {
        printf("%d-", tmp->key);
        tmp = tmp->next;
    }
    printf("\n");
}

int main() {

    // create a linked list
    int arr_val[] = {7,14,21,28,35,42};
    int arr_size = sizeof(arr_val)/sizeof(int);
    struct node * tmp;
    struct node * target;
    int search_key = 42;  // This is the value we are going to delete.
    int found = 0;

    // read the val from array and put into the linked list.
    for(int i = 0; i < arr_size; i++) {
        tmp = get_node(arr_val[i]);
        tmp->next = head;
        head = tmp;
    }

    // display all nodes values.
    display_nodes(head);

    // delete the value
    target = head;
    while(target) {
        if(target->key == search_key) {
            found = 1;
            break;
        }
        target = target->next;
    }

    if (found == 1){
        // find target's previous node with tmp
        tmp = head;
        while(tmp) {
            if (target == head) {
                head = tmp->next;
                tmp->next = NULL;
                free(tmp);
                break;
            }
            if (tmp->next->key == search_key) {
                tmp->next = tmp->next->next;
                target->next = NULL;
                free(target);
                break;
            }
            tmp = tmp->next;
        }
    } else {
        printf("Can not find the matching number\n");
    }

    display_nodes(head);
}