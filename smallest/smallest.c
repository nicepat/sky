/* Given an array arr[] and an integer K where K is smaller than size of array, the task is to find the Kth smallest element in the given array.
It is given that all array elements are distinct.
arr[] = 7 10 4 3 20 15
K = 4
output is 7
*/

#include <stdio.h>

void print_arr(int arr[], int size) {
    for(int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main() {
    int arr_val[] = {7,10,4,3,20,15};
    int arr_size = sizeof(arr_val)/sizeof(int);
    int K = 3;

    print_arr(arr_val, 6);

    for (int i = 0; i < arr_size; i++) {
        for (int j = i; j < arr_size -1; j++) {
            if (arr_val[i] > arr_val[j]) {
                int tmp = arr_val[i];
                arr_val[i] = arr_val[j];
                arr_val[j] = tmp;
            }
        }
    }

    print_arr(arr_val,6);

    printf("Output is %d\n", arr_val[K-1]);
}