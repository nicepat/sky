#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Find the text1 string in the file, filename.
#define BUFSIZE 256
// hashtable - key: word

// TODO: need to complete for most_used_words

int main() {
    char *filename = "mybook.txt";
    char *text1 = "home";
    char buf[BUFSIZ];
    FILE *fd = fopen(filename, "r");
    printf("I am going to find a string, %s from my book, %s\n", text1, filename);

    if (!fd) {
        printf("file open error");
        exit;
    }

    while ((fgets(buf, BUFSIZE, fd) != NULL)) {
        if (strstr(buf, text1) != NULL) {
            printf("found %s\n", buf);
        }
    }
}